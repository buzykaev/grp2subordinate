# Changelog

## [v0.3.0](https://gitlab.cern.ch/it-cm-lcs/grp2subordinate/-/tree/0.3.0) (2024-02-21)
- New --users parameter to support static list of users to add.

## [v0.2.0](https://gitlab.cern.ch/it-cm-lcs/grp2subordinate/-/tree/0.2.0) (2023-09-29)
- New --users parameter to support static list of users to add.

## [v0.1.1](https://gitlab.cern.ch/it-cm-lcs/grp2subordinate/-/tree/0.1.1) (2020-09-29)
- Align spec file and setup.py version numbers
- Bugfix for timer so that it actually runs more than once.

## [v0.0.1](https://gitlab.cern.ch/it-cm-lcs/grp2subordinate/-/tree/0.0.1) (2020-09-29)
- Initial Release
